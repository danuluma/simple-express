const booksController = (Book) => {
  const getAll = async (req, res) => {
    try {
      const { query } = req;
      const params = {};
      if (query.title) {
        params.title = query.title;
      }
      if (query.author) {
        params.author = query.author;
      }
      if (query.genre) {
        params.genre = query.genre;
      }
      if (query.read) {
        params.read = query.read;
      }
      const books = await Book.find(params).collation({ locale: 'en', strength: 2 });
      return res.json({ books });
    } catch (error) {
      return res.status(500).json({ msg: 'error', error });
    }
  };

  const getOne = (req, res) => {
    const { book } = req;
    return res.status(200).json({ book });
  };

  const post = async (req, res) => {
    try {
      const { body } = req;
      const data = {
        title: '',
        author: '',
        genre: '',
      };
      const errors = {};
      if (body.title && body.title.trim().length) {
        data.title = body.title.trim();
      } else {
        errors.title = 'Title is required';
      }
      if (body.author && body.author.trim().length) {
        data.author = body.author.trim();
      } else {
        errors.author = 'Author is required';
      }
      if (body.genre && body.genre.trim().length) {
        data.genre = body.genre.trim();
      } else {
        errors.genre = 'Genre is required';
      }
      if (Object.keys(errors).length) {
        res.status(400);
        return res.json({ errors });
      }
      const book = new Book(data);
      await book.save();
      res.status(201);
      return res.json(book);
    } catch (error) {
      return res.status(500).json(error);
    }
  };

  const patch = async (req, res) => {
    try {
      const { book, body } = req;
      if (body.title && body.title.trim().length) {
        book.title = body.title.trim();
      }
      if (body.author && body.author.trim().length) {
        book.author = body.author.trim();
      }
      if (body.genre && body.genre.trim().length) {
        book.genre = body.genre.trim();
      }
      if (body.read) {
        book.read = body.read;
      }
      await book.save();
      return res.status(200).json({ book });
    } catch (error) {
      if (error.kind === 'ObjectId') {
        return res.status(404).json({ msg: 'Book with that ID does not exist' });
      }
      return res.status(500).json(error);
    }
  };

  const put = async (req, res) => {
    try {
      const { book, body } = req;
      const errors = {};
      if (body.title && body.title.trim().length) {
        book.title = body.title.trim();
      } else {
        errors.title = 'Title is required';
      }
      if (body.author && body.author.trim().length) {
        book.author = body.author.trim();
      } else {
        errors.author = 'Author is required';
      }
      if (body.genre && body.genre.trim().length) {
        book.genre = body.genre.trim();
      } else {
        errors.genre = 'Genre is required';
      }
      if (Object.keys(errors).length) {
        return res.status(400).json({ errors });
      }
      await book.save();
      return res.status(200).json({ book });
    } catch (error) {
      if (error.kind === 'ObjectId') {
        return res.status(404).json({ msg: 'Book with that ID does not exist' });
      }
      return res.status(500).json(error);
    }
  };

  const remove = async (req, res) => {
    try {
      const { book } = req;
      await book.remove();
      return res.sendStatus(204);
    } catch (error) {
      if (error.kind === 'ObjectId') {
        return res.status(404).json({ msg: 'Book with that ID does not exist' });
      }
      return res.status(500).json(error);
    }
  };
  return {
    getAll,
    getOne,
    post,
    patch,
    put,
    remove,
  };
};

module.exports = booksController;
