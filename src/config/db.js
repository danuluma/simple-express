const mongoose = require('mongoose');

let dbURI;
if (process.env.NODE_ENV === 'Test') {
  console.log('Test Environ');
  dbURI = 'mongodb://127.0.0.1:27017/bookAPITest';
} else {
  console.log('Other');
  dbURI = 'mongodb://127.0.0.1:27017/bookAPI';
}

const connectDB = async () => {
  try {
    await mongoose.connect(dbURI, { useNewUrlParser: true });
    console.log('Connected to mongo db');
  } catch (error) {
    console.log('Error connecting to the database');
    process.exit(1);
  }
};

module.exports = connectDB;
