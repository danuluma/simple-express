require('should');

const request = require('supertest');
const mongoose = require('mongoose');

const app = require('../../server');

const Book = mongoose.model('Book');
const agent = request.agent(app);

describe('Book Integration Test', () => {
  it('should allow a book to be posted', async () => {
    const bookPost = {
      title: 'Deleted',
      author: 'Dann',
      genre: 'Bio',
    };
    const res = await agent
      .post('/api/v1/books')
      .send(bookPost)
      .expect(201);
    res.body.read.should.equal(false);
    res.body.should.have.property('_id');
  });

  afterEach(async () => {
    await Book.deleteMany({}).exec();
  });

  after((done) => {
    mongoose.connection.close();
    app.server.close(done());
  });
});
