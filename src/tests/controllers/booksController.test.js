const should = require('should');
const sinon = require('sinon');
const booksController = require('../../controllers/booksController');

describe('Book Controller Tests', () => {
  describe('Post', () => {
    it('should not allow empty title', () => {
      const Book = () => {
        this.save = () => {};
      };
      const req = {
        body: {
          author: 'Dan',
        },
      };
      const res = {
        status: sinon.spy(),
        send: sinon.spy(),
        json: sinon.spy(),
      };
      const controller = booksController(Book);
      controller.post(req, res);
      res.status.calledWith(400).should.equal(true, `Bad status ${res.status.args[0][0]}`);
      res.json
        .calledWith({ errors: { title: 'Title is required', genre: 'Genre is required' } })
        .should.equal(true, `Bad json ${JSON.stringify(res.json.args[0][0])}`);
    });
  });
});
