const express = require('express');
const connectDB = require('./config/db');

connectDB();

const app = express();
app.use(express.json({ extended: false }));
app.use(express.urlencoded({ extended: false }));

app.get('/', (req, res) => {
  res.status(200).json({ msg: 'Congrats bhcbjw' });
});

app.use('/api/v1', require('./routes/v1'));

const PORT = process.env.PORT || 3001;

app.server = app.listen(PORT, () => {
  console.log(`Server started on port ${PORT}`);
  console.log(`http://localhost:${PORT}`);
});

module.exports = app;
