const Book = require('../models/book');

const getBookById = async (req, res, next) => {
  try {
    const { id } = req.params;
    const book = await Book.findById(id);
    if (book) {
      req.book = book;
      return next();
    }
    return res.status(404).json({ msg: 'Book with that ID does not exist' });
  } catch (error) {
    if (error.kind === 'ObjectId') {
      return res.status(404).json({ msg: 'Book with that ID does not exist' });
    }
    return res.status(500).json(error);
  }
};

module.exports = getBookById;
