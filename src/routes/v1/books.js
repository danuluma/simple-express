const express = require('express');
const Book = require('../../models/book');
const getBookById = require('../../middlewares/getBook');
const booksController = require('../../controllers/booksController');

const router = express.Router();
const controller = booksController(Book);

router.get('/', controller.getAll);

router.get('/:id', getBookById, controller.getOne);

router.post('/', controller.post);

router.patch('/:id', getBookById, controller.patch);

router.put('/:id', getBookById, controller.put);

router.delete('/:id', getBookById, controller.remove);

module.exports = router;
